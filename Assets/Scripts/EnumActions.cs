﻿using UnityEngine;
using System.Collections;

public enum EnumActions {
    NONE = -1,
	ActionA,
    ActionB,
    ActionAB,
    ActionGrabObject,
    ActionTurnAroundPlayer,
    ActionMoveUp,
    ActionMoveDown,
    ActionMoveLeft,
    ActionMoveRight,
    ActionRune,         // enter/exit some rune trigger zone
}


public enum EnumCollectibles
{
    NONE = -1,
    Cat,
}