﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LevelController : MonoBehaviour
{

    public Sequence[] tabSequences;
    public CharModel[] tabPlayers;
    public Animator skullLivesAnimator;
    public Text lblNbLives;
    public InvocationCircle invocationCircle;
    public BlankManager blankManager;
    public ResultUILogic resultUILogic;
    public Animator gameOverAnimator;
	public Animator nowFightAnimator;
	public Transform timerObject;
	public Transform timerProgress;
	public GameObject tutorial;
	public Animator demonAnimator;
	public MeteorSystem meteorSystem;
	public AudioClip demonInvocationAudio;
	public AudioClip demonVictory;
	public AudioClip gameOverAudio;
	public Text lblTextGameOver;

    private int currentLevelIndex;
    private Level[] tabLevels;
    private int nbLives;

    private int lastRdmPlayer = -1;
    private int currentSequenceLength;
    private int currentSequenceIndex;

    void Start()
    {
        nbLives = 5;

        tabLevels = GetComponentsInChildren<Level>();

        blankManager.AddObserver(OnSceneRefresh);

        foreach (CharModel character in tabPlayers)
        {
            character.OnAction += OnActionReceived;
        }

        // init play all BGM
        for (int i = 0; i < tabLevels.Length; i++)
        {
            tabLevels[i].GetComponent<AudioSource>().clip = tabLevels[i].BGM;
            tabLevels[i].GetComponent<AudioSource>().Play();
            tabLevels[i].GetComponent<AudioSource>().volume = 0;
            tabLevels[i].GetComponent<AudioSource>().loop = true;
        }

        InitializeLevel(0);
    }

    private void OnSceneRefresh()
    {
        if (currentSequenceIndex >= currentSequenceLength) NextLevel();
    }

    private void OnActionReceived(ActionData pData)
    {
        if (pData.Type == EnumActions.NONE || pData.ActionStart == false)
            return;

		//if (pData.Type == EnumActions.ActionRune)
			//print ("Rune : " + ((ActionRuneData)pData).RuneIndex);



        bool sequenceIndexCleared = true;

		//print (currentSequenceIndex);
        for (int compteur = 0; compteur < tabSequences.Length; compteur++)
        {
            if (tabLevels[currentLevelIndex].isCoop)
            {
                SequenceItem item = tabSequences[compteur].GetSequenceItemAt(currentSequenceIndex);

				if (item.Action != null && pData.Type == EnumActions.ActionRune)
				{
					print ("RUNE!!  to do: " + item.IndexRune + "     pressed: " +  ((ActionRuneData)pData).RuneIndex);
				}

                if (compteur == pData.ActorIndex)
                {
                    if (item.IsCompleted)
                    {
                        Error("Action already completed");
                        sequenceIndexCleared = false;
                    }
                    else if (item.Action != null && item.Action.action == EnumActions.ActionAB && (pData.Type == EnumActions.ActionA || pData.Type == EnumActions.ActionB))
                    {
                        //item.SetUltimatum(Error);//etre plus permissif, ne rien faire
                        sequenceIndexCleared = false;
                    }
                    else if (item.Action == null || pData.Type != item.Action.action)
                    {
                        if (pData.Type != EnumActions.ActionTurnAroundPlayer && pData.Type != EnumActions.ActionRune) Error("Wrong action");
                        sequenceIndexCleared = false;
                    }
					else if (item.Action != null && pData.Type == EnumActions.ActionRune && item.IndexRune != ((ActionRuneData)pData).RuneIndex)
					{
						//Wrong rune.
						sequenceIndexCleared = false;
					}
                    else
                    {
                        print("Success");
                        item.IsCompleted = true;

                        //check if other action from other player is in sync
                        //if so, tell this sequence item it has 0.25s to happen or else it callbacks Error()
                        for (int i = 0; i < tabSequences.Length; i++)
                        {
                            SequenceItem item2 = tabSequences[i].GetSequenceItemAt(currentSequenceIndex);
                            if (i != pData.ActorIndex && item2.Action != null && item2.IsCompleted == false)
                            {
                                item2.SetUltimatum(Error);
                            }
                        }
                    }
                }
                else
                {
                    if (item.IsCompleted == false && item.Action != null)
                        sequenceIndexCleared = false;
                }
            }
            else
            {
                if (compteur == pData.ActorIndex)
                {
                    SequenceItem item = tabSequences[compteur].GetSequenceNextSequenceItem();
                    if (pData.Type == item.Action.action)
                    {
                        item.IsCompleted = true;

                        //Check for victory :
                        if (tabSequences[compteur].GetSequenceNextSequenceItem() == null)
                        {
                            Victory(compteur);
                        }
                    }
                }
            }
        }

        if (sequenceIndexCleared)
        {
            print("Next sequence");

            if (tabLevels[currentLevelIndex].isCoop)
            {
                //call note sound :
                string notePath = "Audio/Notes/";
                if (pData.ActorIndex == 0) notePath += "VOICES_NOTE_";
                if (pData.ActorIndex == 1) notePath += "SYNTHS_NOTE_";
                if (pData.ActorIndex == 2) notePath += "PERCS_NOTE_";
                notePath += tabLevels[currentLevelIndex].tabAudioSequence[currentSequenceIndex];
                SoundManager.Instance.PlaySound(notePath);

                currentSequenceIndex++;

                if (currentSequenceIndex >= currentSequenceLength) SoundManager.Instance.PlaySound(tabLevels[currentLevelIndex].victoryAudio);
                else SoundManager.Instance.PlaySound(notePath);
            }


            //invocationCircle will check for end of level.
            int seqPos = currentSequenceIndex;
            //if not in coop, current progress is defined by the leading player :
            if (tabLevels[currentLevelIndex].isCoop == false)
            {
                for (int i = 0; i < tabSequences.Length; i++)
                {
                    if (tabSequences[i].GetCurrentSequenceIndex() > seqPos)
                        seqPos = tabSequences[i].GetCurrentSequenceIndex();
                }
            }

			if (seqPos >= currentSequenceLength) {
				//end of level :

				//stop timer 
				iTween.Stop (gameObject);

				StopCoroutine ("MeteorCoroutine");
			}

			invocationCircle.throwActionFX(seqPos, currentSequenceLength, currentLevelIndex);
        }
    }

    public void Error(string pError)
    {
        print("Error : " + pError);

		if (tabLevels[currentLevelIndex].isCoop && currentLevelIndex > 0)
        {
			if (nbLives > 0)
			{
				nbLives--;
				skullLivesAnimator.SetTrigger ("Lose");
				lblNbLives.text = nbLives.ToString ();
				if (nbLives == 0) {
					GameOver ();
				}
			}
        }


        if (pError == "unsync")
        {
            for (int i = 0; i < tabSequences.Length; i++)
            {
                SequenceItem item = tabSequences[i].GetSequenceItemAt(currentSequenceIndex);
                if (item.Action != null)
                {
                    item.IsCompleted = false;
                }
            }
        }
    }

    private void NextLevel()
    {
        if (tabLevels.Length > currentLevelIndex + 1)
        {
            InitializeLevel(currentLevelIndex + 1);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            NextLevel();
        }
    }

    private IEnumerator FadeVolume(AudioSource source, float from, float to, float time)
    {
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / time)
        {
            source.volume = Mathf.Lerp(from, to, t);
            yield return null;
        }
        yield return 0;
    }

    private void InitializeLevel(int pLevelIndex)
    {
		
        currentLevelIndex = pLevelIndex;
        for (int compteur = 0; compteur < tabLevels.Length; compteur++)
        {
            tabLevels[compteur].IsActive = (compteur == currentLevelIndex);
            // BGM
            StartCoroutine(FadeVolume(tabLevels[compteur].GetComponent<AudioSource>(), (compteur == currentLevelIndex ? 0 : tabLevels[compteur].GetComponent<AudioSource>().volume), (compteur == currentLevelIndex ? 1 : 0), 1f));
        }

		tutorial.SetActive (currentLevelIndex == 0);

        currentSequenceIndex = 0;

        //Clear previous sequence:
        for (int compteurSequences = 0; compteurSequences < 3; compteurSequences++)
        {
            tabSequences[compteurSequences].Clear();
        }

        Level currentLevel = tabLevels[currentLevelIndex];
        currentSequenceLength = currentLevel.tabAudioSequence.Length;

        if (currentLevel.isCoop == false)
        {
			StartCoroutine (NowFightCoroutine ());
        }

		StopCoroutine ("MeteorCoroutine");
		if(currentLevel.maxTimeSpawnMeteorite > 0)StartCoroutine ("MeteorCoroutine");

		iTween.Stop (gameObject);
		bool hasTimerAndCanLoseLives = (currentLevelIndex != 0 && currentLevel.isCoop);
		timerObject.gameObject.SetActive (hasTimerAndCanLoseLives);
		lblNbLives.gameObject.SetActive (hasTimerAndCanLoseLives);
		if (hasTimerAndCanLoseLives)
		{
			iTween.ValueTo (gameObject, iTween.Hash("time", currentLevel.time, "from", 0, "to", 1, "onupdate", "UpdateTimer", "OnComplete", "TimerRanOut"));
		}



        //Create new sequence for current level :
        for (int i = 0; i < currentSequenceLength; i++)
        {
            int maxStretch = currentSequenceLength - i;
            float stretchProbability = currentLevel.stretchProbability;
            Action randomAction = currentLevel.tabPossibleActions[Random.Range(0, currentLevel.tabPossibleActions.Length)];

            if (currentLevel.isCoop)
            {
                bool isForEveryone = (i % 4 == 0 && i != 0 && randomAction.CanBeForEveryone);
                if (isForEveryone)
                {
                    for (int compteurSequences = 0; compteurSequences < 3; compteurSequences++)
                    {
                        tabSequences[compteurSequences].AddSequenceItem(randomAction, maxStretch, stretchProbability);
                    }
                }
                else {
                    int rdmPlayer = Random.Range(0, tabSequences.Length);
                    //Reduce chance to have always the same players playing :
                    if (rdmPlayer == lastRdmPlayer)
                        rdmPlayer = Random.Range(0, tabSequences.Length);

                    lastRdmPlayer = rdmPlayer;

                    for (int compteurSequences = 0; compteurSequences < 3; compteurSequences++)
                    {
                        if (compteurSequences == rdmPlayer)
                            tabSequences[compteurSequences].AddSequenceItem(randomAction, maxStretch, stretchProbability);
                        else
                            tabSequences[compteurSequences].AddSequenceItem(null, maxStretch, stretchProbability);
                    }
                }
            }
            else
            {
                //In versus mode, everyone has the same actions to complete but we shuffle the order for each player
                for (int j = 0; j < tabSequences.Length; j++)
                {
                    tabSequences[j].AddSequenceItem(randomAction, maxStretch, stretchProbability);
                }
            }
        }

        if (currentLevel.isCoop == false)
        {
            for (int i = 0; i < tabSequences.Length; i++)
            {
                tabSequences[i].ShuffleSequence();
            }
        }

		//Meteor
		FindObjectOfType<MeteorSystem> ().cleanMeteors ();
    }

	private void UpdateTimer(float pTimerProgress)
	{
		timerProgress.transform.localScale = new Vector3 (1, pTimerProgress, 0);
	}

	private IEnumerator MeteorCoroutine()
	{
		float wait = Random.Range (tabLevels [currentLevelIndex].minTimeSpawnMeteorite, tabLevels [currentLevelIndex].maxTimeSpawnMeteorite);
		yield return new WaitForSeconds (wait);
		meteorSystem.throwMeteorOnPlayers ();
		print ("METEOR!");

		StartCoroutine ("MeteorCoroutine");
	}

	private IEnumerator NowFightCoroutine()
	{
		yield return new WaitForSeconds (1);
		
		demonAnimator.gameObject.SetActive (true);
		SoundManager.Instance.PlaySound (demonInvocationAudio);

		yield return new WaitForSeconds (2);

		nowFightAnimator.SetTrigger("Fight");

	}

	private void TimerRanOut()
	{
		if (currentLevelIndex < 5)
		{
			lblTextGameOver.text = "Time Out";
			GameOver ();
		}

	}

    public void Victory(int pIndexWinnerPlayer)
    {
		demonAnimator.SetTrigger ("Win");
		SoundManager.Instance.PlaySound (demonVictory);
        resultUILogic.ShowResult(pIndexWinnerPlayer);
    }

    public void GameOver()
    {
        gameOverAnimator.SetTrigger("GameOver");
		SoundManager.Instance.PlaySound (gameOverAudio);
    }

    public void Reload()
    {
        Application.LoadLevel("Splash");
    }
}
