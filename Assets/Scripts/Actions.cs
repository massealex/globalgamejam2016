﻿using UnityEngine;
using System.Collections;

// base action
[System.Serializable]
public class ActionData
{
    public int ActorIndex;
    public EnumActions Type;
    public bool ActionStart = true;        // for actions with a start and an end (A,B,AB)
}

[System.Serializable]
public class ActionTurnData: ActionData
{
    public int TargetIndex;
}

[System.Serializable]
public class ActionGrabData : ActionData
{
    public EnumCollectibles Collectible;
}

[System.Serializable]
public class ActionRuneData : ActionData
{
    public int RuneIndex;
}


