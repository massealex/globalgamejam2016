﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

	public string[] tabAudioSequence;
	public AudioClip victoryAudio;
    public AudioClip BGM;
    public float stretchProbability;
	public bool isCoop = true;
	public float time;
	public Action[] tabPossibleActions;
	public float minTimeSpawnMeteorite;
	public float maxTimeSpawnMeteorite;

	void Start () {
	
	}

	public bool IsActive
	{
		set { transform.GetChild (0).gameObject.SetActive (value); }
	}
}
