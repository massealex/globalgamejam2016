﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class BlankManager : MonoBehaviour {
	private bool isAppearing = false;
	private bool isDisappearing = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (this.isAppearing) {
			float alpha = this.GetComponent<CanvasGroup> ().alpha;

			if (alpha < 1) {
				alpha += Time.deltaTime / 1.5f;
			} else {
				alpha = 1;
				this.isAppearing = false;
				StartCoroutine(WaitForRemoveBlankRoutine() );
			}
			this.GetComponent<CanvasGroup> ().alpha = alpha;
		} else if (this.isDisappearing) {
			float alpha = this.GetComponent<CanvasGroup> ().alpha;
			if (alpha > 0) {
				alpha -= Time.deltaTime / 1.5f;
			} else {
				alpha = 0;
				this.isDisappearing = false;
			}
			this.GetComponent<CanvasGroup> ().alpha = alpha;
		}
	}


	IEnumerator WaitForRemoveBlankRoutine () {
		yield return new WaitForSeconds (0.5f);
		
		this.isDisappearing = true;
		if(OnEnding != null) OnEnding ();
	}

	public void triggerOnEnding () {
		if(OnEnding != null) OnEnding ();
	}

	public void ShowBlank () {
		this.isAppearing = true;
	}

	public delegate void StatusUpdateEndScene();
	public event StatusUpdateEndScene OnEnding = null;
	
	public void AddObserver(StatusUpdateEndScene pMethod)
	{
		OnEnding = pMethod;
	}

}
