﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ResultUILogic : MonoBehaviour
{

    public static List<string> MonksNames = new List<string>() { "Huey", "Dewey", "Louie" };
    public static List<string> DevilNames = new List<string>() { "Belzebuth", "Satan", "Lucifer" };

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void ShowResult(int winnerIdx)
    {
        GetComponent<Animation>().Play("ShowResult");         // fade animation (if we ever need it)

        string text = "Starring\n";
        for (int i = 0; i < 3; i++)
        {
            if (winnerIdx == i)
                continue;

            text += "\nPlayer #" + (i + 1) + " ................................. " + MonksNames[i];
        }
        text += "\n\nAnd player #" + (winnerIdx + 1) + " as " + DevilNames[winnerIdx] + ", LORD OF HELL";

        GameObject.Find("WinnerTxt").GetComponent<Text>().text = text;
    }

    public void EndGame()
    {
        Application.LoadLevel("Splash");
        GetComponent<Animation>().Play("Hide");         // fade animation (if we ever need it)
    }
}
