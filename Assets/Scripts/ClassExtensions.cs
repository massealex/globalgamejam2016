﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class ClassExtensions
{
    public static void Shuffle<T>(this List<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static void DestroyGameObjects<T>(this List<T> pList, bool pEmptyList = true)
    {
        for(int compteur = 0; compteur < pList.Count; compteur++)
        {
            if(pList[compteur] is MonoBehaviour)
            {
                MonoBehaviour obj = pList[0] as MonoBehaviour;
                UnityEngine.GameObject.Destroy(obj.gameObject);
                if (pEmptyList)
                {
                    pList.RemoveAt(0);
                    compteur--;
                }
            }
        }
    }

    public static Transform FindGrandChild(this Transform subject, string name)
    {
        Transform result = subject.FindChild(name);

        if (result == null)
        {
            foreach (Transform child in subject)
            {
                result = child.FindGrandChild(name);
                if (result != null) return result;
            }
        }

        return result;
    }
}