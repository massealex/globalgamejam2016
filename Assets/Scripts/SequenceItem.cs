﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SequenceItem : MonoBehaviour {

	private const int MIN_TIME_AFTER_STOP_STRETCH = 1;

	public GameObject panContent;
	public Image sprite;
	public Transform stretchPanel;

	private bool isCompleted;
	private Action action;
	private int indexRune;

	void Start ()
	{

	}

	//Returns strecth sequence length
	public int Initialize(Action pAction, int pMaxStretch, float pStretchProbability)
	{
		action = pAction;

		bool isVisible = pAction != null;
		panContent.SetActive (isVisible);

		if(isVisible)
		{
			if (pAction is ActionRune) {
				indexRune = Random.Range (0, ((ActionRune)pAction).tabSpriteRunes.Length);
				sprite.sprite = ((ActionRune)pAction).tabSpriteRunes [indexRune];
			}
			else
			{
				indexRune = -1;
				sprite.sprite = pAction.sprite;
			}

			if (pAction.CanStretch && pMaxStretch >= 4 && Random.Range(0, 1f) <= pStretchProbability)
			{
				int rdmStretchLength = Random.Range (4, 7);
				if (rdmStretchLength > pMaxStretch) rdmStretchLength = pMaxStretch;

				stretchPanel.transform.localScale = new Vector3 (rdmStretchLength, 1, 1);

				return rdmStretchLength + MIN_TIME_AFTER_STOP_STRETCH;//cant do other action sometimes after stoping a stretch
			}
		}

		return 0;
	}

	public bool IsCompleted
	{
		get {
			return isCompleted;
		}
		set {

			if (value) {
				iTween.PunchScale (gameObject, new Vector3 (0, 1, 0), 1);
				panContent.GetComponent<Image> ().color = new Color (0, 0, 0, 0.35f);
			} else {
				panContent.GetComponent<Image> ().color = new Color (1, 1, 1, 1);
			}

			isCompleted = value;
		}
	}

	public Action Action
	{
		get {
			return action;
		}
	}

	public delegate void StatusUpdateUltimatum(string pError);
	public event StatusUpdateUltimatum OnUltimatumFailed = null;

	public void SetUltimatum(StatusUpdateUltimatum pMethod)
	{
		OnUltimatumFailed = pMethod;
		StopCoroutine ("UltimatumRoutine");
		StartCoroutine ("UltimatumRoutine");
	}

	private IEnumerator UltimatumRoutine()
	{
		yield return new WaitForSeconds(0.25f);

		if(isCompleted == false)
		{
			if (OnUltimatumFailed != null) OnUltimatumFailed("unsync");
		}
	}

	public int IndexRune
	{
		get { return indexRune;}
	}
}
