﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sequence : MonoBehaviour {

	public Transform sequenceItemModel;

	private List<SequenceItem> lstSequence = new List<SequenceItem>();
	private int inStretchUntil;

	void Awake ()
	{
		sequenceItemModel = transform.GetChild (0).GetChild (0);
		sequenceItemModel.gameObject.SetActive(false);
	}

	public void Clear()
	{
		inStretchUntil = -1;

		foreach (SequenceItem item in lstSequence)
		{
			Destroy (item.gameObject);
		}

		lstSequence.Clear ();
	}
	
	public void AddSequenceItem(Action pAction, int pMaxStretch, float pStretchProbability)
	{
		Transform newTr = (Transform)Transform.Instantiate (sequenceItemModel);
		newTr.SetParent (sequenceItemModel.parent);
		newTr.localScale = Vector3.one;
		newTr.gameObject.SetActive (true);

		SequenceItem sequenceItem = newTr.GetComponent<SequenceItem> ();
		lstSequence.Add (sequenceItem);

		//Block actions when in stretch :
		if (lstSequence.Count < inStretchUntil) pAction = null;

		int stretchLength = sequenceItem.Initialize (pAction, pMaxStretch, pStretchProbability);
		if(stretchLength > 0) inStretchUntil = lstSequence.Count + stretchLength;

	}

	public SequenceItem GetSequenceItemAt(int pIndex)
	{
		return lstSequence[pIndex];
	}

	public void ShuffleSequence()
	{
		//lstSequence.Shuffle ();

		for (int compteur = 0; compteur < 100; compteur++)
		{
			int rdmIndex = Random.Range(0, lstSequence.Count);
			SequenceItem item = lstSequence[rdmIndex];
			lstSequence.RemoveAt (rdmIndex);
			lstSequence.Insert (0, item);
			item.GetComponent<RectTransform>().SetAsFirstSibling();
		}
	}

	public SequenceItem GetSequenceNextSequenceItem()
	{
		for (int compteur = 0; compteur < lstSequence.Count; compteur++)
		{
			if (lstSequence [compteur].IsCompleted == false) return lstSequence [compteur];
		}

		return null;
	}

	public int GetCurrentSequenceIndex()
	{
		for (int compteur = 0; compteur < lstSequence.Count; compteur++)
		{
			if (lstSequence [compteur].IsCompleted == false) return compteur - 1;
		}

		return 0;
	}
}
