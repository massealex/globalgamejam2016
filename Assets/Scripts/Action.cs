﻿using UnityEngine;
using System.Collections;

public class Action : MonoBehaviour {

	public EnumActions action;
	public string displayLabel;
	public Sprite sprite;

	void Start () {
	
	}

	public bool CanBeForEveryone
	{
		get {
			return action == EnumActions.ActionA ||
			action == EnumActions.ActionB;
		}
	}

	public bool CanStretch
	{
		get {
			return action == EnumActions.ActionA ||
			action == EnumActions.ActionB ||
			action == EnumActions.ActionAB ||
			action == EnumActions.ActionRune;
		}
	}

}
