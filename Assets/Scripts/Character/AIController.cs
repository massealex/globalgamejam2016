﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AIBehaviour
{
    NONE = -1,
    RunAway,            // runs away when someone approaches
}

[System.Serializable]
public class AISettings
{
    public float AIThinkTime = .5f;
    public List<AIBehaviour> Behaviours;
    public float DistThres;         // distance threshold for behaviors (TODO: list of distance/struct if different thresholds)
}

public class AIController : CharController
{

    // REFERENCES

    // VARIABLES
    protected float CurThinkTime = 0f;

    // SETTINGS
    public AISettings AI;

    // EVENTS


    // Use this for initialization

    // Update is called once per frame
    protected override void Update()
    {
        CurThinkTime += Time.deltaTime;
        // AI action calls
        if (CurThinkTime > AI.AIThinkTime)
        {
            CurThinkTime -= AI.AIThinkTime;

            Vector2 moveinput = Vector2.zero;

            // RUN AWAY behaviour
            if (AI.Behaviours.FindAll(x => x == AIBehaviour.RunAway).Count > 0)
            {
                CharModel opponent = null;
                foreach (CharModel mod in FindObjectsOfType<CharModel>())
                {
                    if (mod != Model && (mod.MyTransform.position - Model.MyTransform.position).magnitude < AI.DistThres)
                    {
                        if (opponent == null || (mod.MyTransform.transform.position - Model.MyTransform.position).magnitude < (opponent.MyTransform.transform.position - Model.MyTransform.position).magnitude)
                            opponent = mod;                        
                    }
                }

                if (opponent != null)
                    moveinput = Model.MyTransform.position - opponent.MyTransform.position;

            }

            Model.CallMove(moveinput);

        }

    }

}
