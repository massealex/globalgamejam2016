﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class JoySettings
{
    public string xAxis = "HorizontalJoy1";        // for joystick only
    public string yAxis = "VerticalJoy1";       // for joystick only
    public KeyCode ActionA = KeyCode.Joystick1Button0;
    public KeyCode ActionB = KeyCode.Joystick1Button1;
}

// to add to 1 player only (joystick player)
public class JoystickController : CharController
{

    // REFERENCES

    // VARIABLES

    // SETTINGS
    public JoySettings Joy = new JoySettings();

    // EVENTS


    protected override Vector3 GetMove()
    {
        Vector2 moveinput = Vector2.zero;
        if (Joy.xAxis.Length > 0 && Joy.yAxis.Length > 0)
        {
            // joystick override
            moveinput.x = Input.GetAxis(Joy.xAxis);
            moveinput.y = -Input.GetAxis(Joy.yAxis);
        }

        if (moveinput.magnitude < CharModel.MoveThreshold)
            moveinput = base.GetMove();

        return moveinput;
    }

    protected override bool GetAction(out bool a, out bool b)
    {
        bool ret = (Input.GetKeyDown(Joy.ActionA) || Input.GetKeyDown(Joy.ActionB) || Input.GetKeyUp(Joy.ActionA) || Input.GetKeyUp(Joy.ActionB));
        a = Input.GetKey(Joy.ActionA);
        b = Input.GetKey(Joy.ActionB);

        if (ret == false)
        {
            ret = base.GetAction(out a, out b);
        }
        return ret;
    }


}
