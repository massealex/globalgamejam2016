﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

// DELEGATES

// DATA
[System.Serializable]
public class CollectibleSettings
{
    public EnumCollectibles Collectible;
    public float ResetTime = 2f;        // time to reappear

    // for view
    public AudioClip SoundOnDestroy;
    public AudioClip SoundOnCatch;
}

public class CollectibleModel : CharModel
{
    // CONST

    // REFERENCES

    // VARIABLES

    // SETTINGS
    public CollectibleSettings CollecSettings;

    // EVENTS


    protected override void Kill()
    {

        GameObject catBurnParticle = Instantiate(Resources.Load("CharMesh/" + "CatBurnParticule"), MyTransform.position, Quaternion.identity) as GameObject;
        catBurnParticle.transform.Rotate(-90, 0, 0);
        catBurnParticle.GetComponent<ParticleSystem>().enableEmission = true;

        // SFX
        if (CollecSettings.SoundOnDestroy != null)
            SoundManager.Instance.PlaySoundAt(CollecSettings.SoundOnDestroy, MyTransform.position);
    }


}