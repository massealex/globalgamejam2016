﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

// DELEGATES
public delegate void ActionDelegate(ActionData data);

// DATA 
[System.Serializable]
public class CharModelSettings
{
    public float Accel = 24;
    public float Friction = 20;
    public float MaxSpeed = 4f;
    public float InvulnerableTime = 3f;

    public int Index;
}

public class CharModel : MonoBehaviour
{
    // CONST
    public const float TriggerCollisionMemoryTime = 2f;
    public const int TriggerCountPerChar = 4;
    public const float MoveThreshold = .5f;
    public const float MinSpawnRange = 4f;      // don't spawn collectibles closer than this to another char
    // REFERENCES
    protected CharView View;
    protected CharController Controller;
    [HideInInspector]
    public Transform MyTransform;           // reference for transform for all components of this character

    // ingame refs
    protected Dictionary<Collider2D, float> TriggersTimes = new Dictionary<Collider2D, float>();
    protected CollectibleModel GrabbedCollectible;

    // VARIABLES
    protected Vector2 MoveInput;         // access by controller
    protected EnumActions CurAction = EnumActions.NONE;
    protected Vector2 Speed;
    protected bool IsInvulnerable = false;

    private bool isStuned;
    private bool isControllable = true;
    private float stunEndTimestamp = 0;

    // SETTINGS
    public CharModelSettings Settings = new CharModelSettings();

    // EVENTS
    public ActionDelegate OnAction;


    // Use this for initialization
    void Awake()
    {
        // init refs
        View = GetComponent<CharView>();
        Controller = GetComponent<CharController>();
        MyTransform = transform;

        // test delegate (remove TODO)
        //OnAction += ActionDelegTest;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.isStuned || isControllable == false)
            return;
        // move: accel
        if (MoveInput.magnitude > MoveThreshold)
        {
            // on 1st time: play the idle loop
            if (Speed.magnitude < Mathf.Epsilon)
            {
                //View.PlayAnim("Char_march");
                if (View.Anim != null)
                    View.Anim.SetBool("Walking", true);
            }

            Speed += Settings.Accel * MoveInput * Time.deltaTime;
        }
        else
        {
            Vector2 friction = Speed.normalized * Settings.Friction * Time.deltaTime;
            if (friction.magnitude > Speed.magnitude)
            {
                // on 1st time: play the idle loop
                if (Speed.magnitude > Mathf.Epsilon && CurAction == EnumActions.NONE)
                    //View.PlayAnim("NONE");
                    if (View.Anim != null)
                        View.Anim.SetBool("Walking", false);

                Speed = Vector2.zero;
            }
            else
                Speed -= friction;
        }

        // check speed limit
        if (Speed.magnitude > Settings.MaxSpeed)
            Speed = Speed.normalized * Settings.MaxSpeed;

        // compute move
        Vector3 move = new Vector3(Speed.x, Speed.y, 0) * Time.deltaTime; // new in update (dirty) --> better use of transform in 2d TODO ?)

        // rotate
        if (MoveInput.magnitude > Mathf.Epsilon)
        {
            View.LookRotation(MoveInput);
        }

        // apply on transform
        MyTransform.position += move;
    }

    public void CallMove(Vector2 input)
    {
        MoveInput = input;
        if (MoveInput.magnitude > 1)
            MoveInput.Normalize();
    }

    public void CallAction(EnumActions action)
    {
        if (this.isStuned || isControllable == false)
            return;
        // delegate (note: might want to run checks first, then call delegates ? TODO)
        if (action != CurAction && OnAction != null)
        {
            OnAction(new ActionData() { ActorIndex = Settings.Index, Type = CurAction, ActionStart = false });  // declare end of old action
            OnAction(new ActionData() { ActorIndex = Settings.Index, Type = action, ActionStart = true });  // declare start of new action

            // play anim
            //            View.PlayAnim(action.ToString());
            if (View.Anim != null)
            {
                View.Anim.SetBool("A", action == EnumActions.ActionA || action == EnumActions.ActionAB);
                View.Anim.SetBool("B", action == EnumActions.ActionB || action == EnumActions.ActionAB);

                // SFX
                if (action == EnumActions.ActionA)
                    SoundManager.Instance.PlaySoundAt("Audio/SFX/SFX_VoiceA", MyTransform.position);
                if (action == EnumActions.ActionB)
                    SoundManager.Instance.PlaySoundAt("Audio/SFX/SFX_VoiceB", MyTransform.position);
            }

        }

        // set action
        CurAction = action;

    }

    public EnumActions GetCurAction()
    {
        return CurAction;
    }

    protected IEnumerator SetInvulnerable(float time)
    {
        // play invulnerable anim
        IsInvulnerable = true;
        View.ShowInvulnerable(IsInvulnerable);

        yield return new WaitForSeconds(time);

        // stop invulnerable anim
        IsInvulnerable = false;
        View.ShowInvulnerable(IsInvulnerable);
    }

    protected IEnumerator DisableForTime(float time)
    {
        gameObject.transform.SetParent(null);
        isControllable = true;
        gameObject.SetActive(false);

        yield return new WaitForSeconds(time);
        // reset position & activate
        gameObject.SetActive(true);

        // set position random (if floor area defined)
        if (GameObject.Find("FloorArea") != null)
        {
            Transform floorArea = GameObject.Find("FloorArea").transform;
            Vector3 newPos = Vector3.zero;
            for (int i = 0; i < 10; i++)      // max 10 tries - HARDCODE
            {
                newPos.x = Random.Range(-floorArea.localScale.x / 2 + floorArea.localPosition.x, floorArea.localScale.x / 2 + floorArea.localPosition.x);
                newPos.y = Random.Range(-floorArea.localScale.y / 2 + floorArea.localPosition.y, floorArea.localScale.y / 2 + floorArea.localPosition.y);

                // check against character
                bool ok = true;
                foreach (CharModel chara in FindObjectsOfType<CharModel>())
                {
                    if ((chara.MyTransform.position - newPos).magnitude < MinSpawnRange)
                    {
                        ok = false;
                        break;
                    }
                }

                // break if found a ok position
                if (ok)
                    break;
            }

            // set new pos
            MyTransform.position = newPos;

        }

        // set invulnerable
        SetInvulnerable(Settings.InvulnerableTime);

        yield return 0;
    }

    protected virtual void Kill()
    {
        // do nothing (players don't die~)
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        // doesn't collide if invulnerable
        if (IsInvulnerable == true)
            return;

        // CIRCLE
        if (other.GetComponent<CircleModel>() != null)
        {
            // if carrying an item: destroy it and send event info
            if (GrabbedCollectible != null)
            {
                if (OnAction != null)
                    OnAction(new ActionGrabData() { ActorIndex = Settings.Index, Type = EnumActions.ActionGrabObject, Collectible = GrabbedCollectible.CollecSettings.Collectible });

                StartCoroutine(GrabbedCollectible.DisableForTime(GrabbedCollectible.CollecSettings.ResetTime));
                GrabbedCollectible.Kill();
                GrabbedCollectible = null;

            }
        }

        // RUNE
        else if (other.GetComponent<RuneModel>() != null)
        {
            RuneModel rune = other.GetComponent<RuneModel>();
            rune.View.PlayAnim("RuneOn");
            if (OnAction != null)
                OnAction(new ActionRuneData() { ActorIndex = Settings.Index, Type = EnumActions.ActionRune, RuneIndex = rune.RuneSets.Index, ActionStart = true });

            // SFX
            SoundManager.Instance.PlaySoundAt(rune.RuneSets.SFX, MyTransform.position);
        }

        // COLLECTIBLE
        else if (other.GetComponent<CollectibleModel>() != null)
        {
            // if carrying an item: release it
            if (GrabbedCollectible != null)
            {
                GrabbedCollectible.MyTransform.SetParent(null);
                GrabbedCollectible.isControllable = true;       // give back control to the object
                GrabbedCollectible = null;
            }

            // grab new collectible (set child of sprite ==> gets rotation applied too)
            other.transform.SetParent(View.Sprite.transform);
            GrabbedCollectible = other.GetComponent<CollectibleModel>();
            GrabbedCollectible.isControllable = false;

            // play SFX
            if (GrabbedCollectible.CollecSettings.SoundOnDestroy != null)
                SoundManager.Instance.PlaySoundAt(GrabbedCollectible.CollecSettings.SoundOnCatch, GrabbedCollectible.MyTransform.position);
        }
        // ELSE: TRIGGER
        else
        {
            // refresh time if existing
            if (TriggersTimes.ContainsKey(other) == true)
            {
                TriggersTimes[other] = Time.time;
            }
            // else add to list
            else
            {
                TriggersTimes.Add(other, Time.time);
            }

            // refresh all times (better here than in updates)
            float oldesttime = Time.time - TriggerCollisionMemoryTime;
            for (int i = TriggersTimes.Count - 1; i >= 0; i--)        // will remove items --> descending through the dictionary rather than using enumerator
            {
                if (TriggersTimes.ElementAt(i).Value < oldesttime)
                    TriggersTimes.Remove(TriggersTimes.ElementAt(i).Key);
            }

            // check if this fits any requirement for action
            foreach (CharModel mod in FindObjectsOfType<CharModel>())
            {
                if (mod == this)
                    continue;

                int count = 0;
                foreach (KeyValuePair<Collider2D, float> pair in TriggersTimes)
                {
                    if (pair.Key.transform.parent == mod.MyTransform)
                        count++;
                }


                // delegate if reached the count (no direction order forced a the moment)
                if (count >= TriggerCountPerChar)
                {
                    if (OnAction != null)
                        OnAction(new ActionTurnData() { ActorIndex = Settings.Index, Type = EnumActions.ActionTurnAroundPlayer, TargetIndex = mod.Settings.Index });

                    // reset all mentions of opponent triggers
                    for (int i = TriggersTimes.Count - 1; i >= 0; i--)        // will remove items --> descending through the dictionary rather than using enumerator
                    {
                        if (TriggersTimes.ElementAt(i).Key.transform.parent == mod.MyTransform)
                            TriggersTimes.Remove(TriggersTimes.ElementAt(i).Key);
                    }
                }
            }
        }
    }

    //Gestion Stunned
    public void SetStuned(float duration)
    {
        this.isStuned = true;
        View.PlayStuned(true);
        StartCoroutine(StunRoutine(duration));
    }

    public bool GetIsStuned()
    {
        return this.isStuned;
    }

    IEnumerator StunRoutine(float duration)
    {
        yield return new WaitForSeconds(duration);

        this.isStuned = false;
        View.PlayStuned(false);
        //Debug.Log ("stunEndTimestamp Stun");
    }



    void OnTriggerExit2D(Collider2D other)
    {
        // RUNE
        if (other.GetComponent<RuneModel>() != null)
        {
            RuneModel rune = other.GetComponent<RuneModel>();
            rune.View.PlayAnim("RuneOff");

            if (OnAction != null)
                OnAction(new ActionRuneData() { ActorIndex = Settings.Index, Type = EnumActions.ActionRune, RuneIndex = rune.RuneSets.Index, ActionStart = false });
        }

    }


    //
    // DELEGATE TESTS
    //
    protected void ActionDelegTest(ActionData data)
    {
        Debug.Log("[TEST] Player " + data.ActorIndex + " " + (data.ActionStart ? "started" : "ended") + " action " + data.Type);

        // for test
        if (data.Type == EnumActions.ActionGrabObject)
            FindObjectOfType<ResultUILogic>().ShowResult(2);
    }
}