﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

// DELEGATES

// DATA
[System.Serializable]
public class RuneSettings
{
    public int Index = 0;

    public AudioClip SFX;

}

public class RuneModel : CharModel
{
    // CONST

    // REFERENCES

    // VARIABLES

    // SETTINGS
    public RuneSettings RuneSets;

    // EVENTS

}