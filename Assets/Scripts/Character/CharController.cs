﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class KeySettings
{
    //public string OverrideXAxis = "";        // for joystick only
    //public string OverrideYAxis = "";       // for joystick only
    public KeyCode Left = KeyCode.LeftArrow;
    public KeyCode Right = KeyCode.RightArrow;
    public KeyCode Up = KeyCode.UpArrow;
    public KeyCode Down = KeyCode.DownArrow;
    public KeyCode ActionA = KeyCode.F;
    public KeyCode ActionB = KeyCode.R;
}

public class CharController : MonoBehaviour
{

    // REFERENCES
    protected CharModel Model;

    // VARIABLES

    // SETTINGS
    public KeySettings Keys = new KeySettings();

    // EVENTS


    // Use this for initialization
    protected virtual void Awake()
    {
        Model = GetComponent<CharModel>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        // move
        Vector2 moveinput = GetMove();
        Model.CallMove(moveinput);

        // detect a change in controls -> call actions (that's a bit ugly -_-;;)
        bool a, b;
        if (GetAction(out a, out b))
        {
            if (a && b)
                Model.CallAction(EnumActions.ActionAB);
            else if (a)
                Model.CallAction(Model.GetCurAction() == EnumActions.ActionAB ? EnumActions.NONE : EnumActions.ActionA);
            else if (b)
                Model.CallAction(Model.GetCurAction() == EnumActions.ActionAB ? EnumActions.NONE : EnumActions.ActionB);
            else
                Model.CallAction(EnumActions.NONE);
        }
    }

    protected virtual Vector3 GetMove()
    {
        return Vector2.right * ((Input.GetKey(Keys.Right) ? 1 : 0) - (Input.GetKey(Keys.Left) ? 1 : 0)) + Vector2.up * ((Input.GetKey(Keys.Up) ? 1 : 0) - (Input.GetKey(Keys.Down) ? 1 : 0));
    }

    protected virtual bool GetAction(out bool a, out bool b)
    {
        bool ret = (Input.GetKeyDown(Keys.ActionA) || Input.GetKeyDown(Keys.ActionB) || Input.GetKeyUp(Keys.ActionA) || Input.GetKeyUp(Keys.ActionB));


        a = Input.GetKey(Keys.ActionA);
        b = Input.GetKey(Keys.ActionB);
        return ret;
    }

    // Set keys for this character (can also use the editor through public settings)
    public void SetKeys(KeySettings keysets)
    {
        Keys = keysets;
    }


}
