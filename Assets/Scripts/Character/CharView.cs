﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharViewSettings
{
    public string MeshName = "TestSprite";
}


public class CharView : MonoBehaviour
{
    // REFERENCES
    private CharModel Model;
    [HideInInspector]
    public GameObject Sprite;
    public Animator Anim;
    public Animation AnimOldSchool;

    // SETTINGS
    public CharViewSettings Settings = new CharViewSettings();

    // VARIABLES
    private bool NeedInit = true;


    // Use this for initialization
    void Awake()
    {
        Model = GetComponent<CharModel>();
    }

    // Update is called once per frame
    void Update()
    {

        // independantly managed init cycle (change possible for some more direct external call if needed)
        if (NeedInit == true)
        {
            Init();
        }
    }

    private void Init()
    {
        // load mesh (temporary, view management TODO)
        Sprite = (GameObject)Instantiate(Resources.Load("CharMesh/" + Settings.MeshName), Model.MyTransform.position, Model.MyTransform.rotation);
        Sprite.transform.SetParent(Model.MyTransform);
        Anim = Sprite.GetComponentInChildren<Animator>();
        AnimOldSchool = Sprite.GetComponent<Animation>();
        NeedInit = false;
    }

    public void Set(CharViewSettings sets)
    {
        Settings = sets;
        NeedInit = true;
    }

    // play anim (used for simple items such as rune. Other items like characters or cat use Animator - set bool/float....)
    public void PlayAnim(string anim, float speed = 1)
    {
        if (AnimOldSchool)
        {
            AnimOldSchool.Play(anim);
            AnimOldSchool[anim].speed = speed;
        }
    }

    public void LookRotation (Vector3 direction)
    {
        if (Sprite != null)
            Sprite.transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
    }

	public void PlayStuned (bool isPlaying) {
		//Debug.Log ("Name " + this.name);
		this.GetComponentInChildren<CharStunedFX> ().SetPlayingStuned (isPlaying);
	}

    public void ShowInvulnerable(bool state)
    {
        // TODO
    }
}
