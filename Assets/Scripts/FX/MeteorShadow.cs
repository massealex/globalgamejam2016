﻿using UnityEngine;
using System.Collections;

public class MeteorShadow : MonoBehaviour {
	bool isAppearing = false;
	bool hasAppeared = false;
	public float appearingTime = 2f;
	
	// Update is called once per frame
	void Update () {
		if (isAppearing && !hasAppeared) {
			Color color = this.GetComponent<SpriteRenderer> ().material.color;
			color.a += appearingTime * Time.deltaTime;
			
			if(color.a >= 1f) {
				color.a = 1f;
				hasAppeared = true;
			}
			
			this.GetComponent<SpriteRenderer> ().material.color = color;
		}
	}
	
	public void appear() {
		isAppearing = true;
	}

}
