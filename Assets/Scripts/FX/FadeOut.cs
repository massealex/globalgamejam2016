﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {
	bool isDisappearing = false;
	bool hasDisappeared = false;
	public float duration = 2f;

	// Update is called once per frame
	void Update () {
		if (isDisappearing && !hasDisappeared) {
			Color color = this.GetComponent<SpriteRenderer> ().material.color;
			color.a -= Time.deltaTime / duration;
			
			if(color.a <= 0f) {
				color.a = 0f;
				hasDisappeared = true;
			}
			
			this.GetComponent<SpriteRenderer> ().material.color = color;
		}
	}
	
	public void fadeOut() {
		isDisappearing = true;
	}
	
	public void fadeOut(float duration) {
		isDisappearing = true;
		this.duration = duration;
	}
}
