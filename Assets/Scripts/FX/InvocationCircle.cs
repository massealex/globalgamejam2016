﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

public class InvocationCircle : MonoBehaviour {
	public ParticleEmitter goodActionEffect;
	public ParticleEmitter endSequenceEffect;
	public GameObject blankImage;
	
	private List<GameObject> players = new List<GameObject>();
	
	private int currentCircleLevel = 0;
	private int currentActionNumber = 0;
	private int levelIndex = 0;
	
	private GameObject Halo;
	
	// Use this for initialization
	void Start () {
		Halo = transform.FindChild("Halo").gameObject;
		Halo.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (0f, 0f, 50f * Time.deltaTime);
		
		if(Input.GetMouseButtonDown(0))
		{
			//this.explodeCircle();
		}
	}
	
	public void explodeCircle () {
		//goodActionEffect.Emit ();
		currentActionNumber++;
		//Pour les tests
		throwActionFX (currentActionNumber, 5, 4);
	}
	
	public void throwActionFX (int currentActionIndex, int actionsCount, int levelIndex) {
		this.levelIndex = levelIndex;
		
		goodActionEffect.GetComponent<EllipsoidParticleEmitter>().Emit();
		
		float ratio = (float)currentActionIndex / (float)actionsCount;
		//Debug.Log ("Ratio " + ratio);
		
		if(ratio > 0 && currentCircleLevel <= 0)
		{
			foreach(GameObject circleFX in GameObject.FindGameObjectsWithTag("CircleLevel1")) {
				circleFX.GetComponent<ParticleSystem>().enableEmission = true;
				
			}
			currentCircleLevel = 1;
		}
		else if(ratio > 0.6 && currentCircleLevel <= 1)
		{
			foreach(GameObject circleFX in GameObject.FindGameObjectsWithTag("CircleLevel2")) {
				circleFX.GetComponent<ParticleSystem>().enableEmission = true;
				
			}
			currentCircleLevel = 2;
		}
		else if(ratio >= 1 && currentCircleLevel <= 2)
		{
			foreach(GameObject circleFX in GameObject.FindGameObjectsWithTag("CircleLevel3")) {
				circleFX.GetComponent<ParticleSystem>().enableEmission = true;
				
			}
			currentCircleLevel = 3;
			endSequenceEffect.GetComponent<EllipsoidParticleEmitter>().Emit();
			if(players.Count >= 3)
				throwEndSequenceAnimation();
			
			// VFX halo
			Halo.SetActive(true);
		}
		
		//Gerer level 5
	}
	
	public void throwEndSequenceAnimation () {
		Debug.Log ("Starting End Sequence Level " + this.levelIndex);
		
		if (this.levelIndex < 4) {
			endSequenceEffect.GetComponent<EllipsoidParticleEmitter> ().Emit ();
			
			StartCoroutine (EndSequenceBlankRoutine ());
			StartCoroutine (EndSequenceFXRoutine (0.15f, (int)10));
			
			//Figer les joueurs
			foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player")) {
				player.GetComponent<CharModel> ().SetStuned (4.8f);
				player.GetComponent<CharView> ().Anim.SetBool ("A", true);
				player.GetComponent<CharView> ().Anim.SetBool ("B", true);
			}
			
		} else if (this.levelIndex >= 4) {
			iTween.ShakePosition(Camera.main.gameObject, new Vector3(0.2f, 0.2f, 0f), 10f);
			blankImage.GetComponent<BlankManager> ().triggerOnEnding();
			foreach(GameObject emitter in GameObject.FindGameObjectsWithTag("DemonInvocation")) {
				if(emitter.GetComponent<ParticleSystem>()) {
					emitter.GetComponent<ParticleSystem>().enableEmission = true;
				}
				if(emitter.GetComponent<EllipsoidParticleEmitter>()) {
					emitter.GetComponent<EllipsoidParticleEmitter>().Emit();
				}
			}
			//Faire tomber des meteorite a eviter
		}
		// VFX halo
		Halo.SetActive(false);
		
	}
	
	IEnumerator EndSequenceFXRoutine (float duration, int quantityEmit) {
		yield return new WaitForSeconds (duration);
		
		quantityEmit--;
		
		endSequenceEffect.GetComponent<EllipsoidParticleEmitter>().Emit();
		
		if(quantityEmit >= 0)
			StartCoroutine(EndSequenceFXRoutine(duration, quantityEmit) );
		//Debug.Log ("stunEndTimestamp Stun");
	}
	
	IEnumerator EndSequenceBlankRoutine () {
		yield return new WaitForSeconds (1.2f);
		
		//Apparition de l'ecran blanc
		//blankImage.GetComponent<Animation> ().Play ("ShowBlank");
		blankImage.GetComponent<BlankManager> ().ShowBlank ();
		//Debug.Log ("ShowBlank");
		
		currentCircleLevel = 0;
		foreach(GameObject circleFX in GameObject.FindGameObjectsWithTag("CircleLevel1")) {
			circleFX.GetComponent<ParticleSystem>().enableEmission = false;
		}
		foreach(GameObject circleFX in GameObject.FindGameObjectsWithTag("CircleLevel2")) {
			circleFX.GetComponent<ParticleSystem>().enableEmission = false;
		}
		foreach(GameObject circleFX in GameObject.FindGameObjectsWithTag("CircleLevel3")) {
			circleFX.GetComponent<ParticleSystem>().enableEmission = false;
		}
		//Player Animation
		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player")) {
			player.GetComponent<CharView>().Anim.SetBool("A", false);
			player.GetComponent<CharView>().Anim.SetBool("B", false);
			player.GetComponent<CharView>().Anim.SetBool("Walking", false);
		}
	}
	
	
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			if(!players.Contains(other.gameObject)) {//.Find(x => x == other.gameObject) == null) {
				players.Add(other.gameObject);
				if(players.Count >= 3 && currentCircleLevel == 3)
					throwEndSequenceAnimation();
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D other) {
		if (other.tag == "Player") {
			if(players.Find(x => x == other.gameObject) != null) {
				players.Remove(other.gameObject);
				//Debug.Log ("Remove Player");
			}
		}
		//Debug.Log ("On trigger exit Invocation circle nb : "+players.Count);
	}
	
	/*
	public static T GetReference<T>(object inObj, string fieldName) where T : class
	{
		return GetField(inObj, fieldName) as T;
	}
	
	
	public static T GetValue<T>(object inObj, string fieldName) where T : struct
	{
		return (T)GetField(inObj, fieldName);
	}
	

	
	public static void SetField(object inObj, string fieldName, object newValue)
	{
		//Get info :
		//inObj.GetType()

		//Set
		FieldInfo info = inObj.GetType().GetField(fieldName);
		if (info != null)
			info.SetValue (inObj, newValue);
		//else
			//Debug.Log ("info = null");
	}
	
	
	private static object GetField(object inObj, string fieldName)
	{
		object ret = null;
		FieldInfo info = inObj.GetType().GetField(fieldName);
		if (info != null)
			ret = info.GetValue(inObj);
		return ret;
	}
	
*/
	
	
}
