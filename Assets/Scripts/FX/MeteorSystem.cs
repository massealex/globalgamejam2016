﻿using UnityEngine;
using System.Collections;

public class MeteorSystem : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void throwMeteorOnPlayers () {
		GameObject[] players = GameObject.FindGameObjectsWithTag ("Player");
		int playerIndex = Random.Range ((int)0, (int)players.Length);
		Vector3 targetPosition = players[playerIndex].transform.position;

		throwMeteorOnPosition (targetPosition);
	}

	public void throwMeteorRandomly () 
	{
		Transform floorArea = GameObject.Find ("FloorArea").transform;
		float posX = Random.Range (- floorArea.localScale.x / 2 + floorArea.localPosition.x, 
		                           floorArea.localScale.x / 2 + floorArea.localPosition.x);
		float posY = Random.Range (- floorArea.localScale.y / 2 + floorArea.localPosition.y, 
		                           floorArea.localScale.y / 2 + floorArea.localPosition.y);
		Vector3 targetPosition = new Vector3 (posX, posY, 0f);
		throwMeteorOnPosition (targetPosition);
	}

	public void throwMeteorOnPosition (Vector3 targetPosition) 
	{
		GameObject myMeteorShadow = Instantiate (Resources.Load("Environnement/" + "MeteorShadowPrefab"), targetPosition, Quaternion.identity) as GameObject;

		Color color = myMeteorShadow.GetComponent<SpriteRenderer> ().material.color;
		color.a = 0.1f;
		myMeteorShadow.GetComponent<SpriteRenderer> ().material.color = color;
		
		myMeteorShadow.GetComponent<MeteorShadow> ().appear();
		//myMeteorShadow.renderer.material.color.a = 1.0;
		
		GameObject myMeteor = Instantiate (Resources.Load("Environnement/" + "MeteorPrefab"), targetPosition, Quaternion.identity) as GameObject;

		myMeteor.transform.Translate (0, 0, -1);

		myMeteor.GetComponent<MeteorBehaviour> ().targetPosition = targetPosition;
		myMeteor.GetComponent<MeteorBehaviour> ().meteorShadow = myMeteorShadow.transform;
	}

	public void cleanMeteors () {
		foreach (MeteorBehaviour meteor in FindObjectsOfType<MeteorBehaviour>()) {
			meteor.gameObject.SetActive(false);
		}
		foreach (MeteorShadow meteor in FindObjectsOfType<MeteorShadow>()) {
			meteor.gameObject.SetActive(false);
		}
	}
}
