﻿using UnityEngine;
using System.Collections;

public class MeteorBehaviour : MonoBehaviour
{
    public Vector3 targetPosition = new Vector3(0f, 0f, 0f);
    public Transform meteorShadow;

    bool isArrived = false;
    // Use this for initialization
    void Start()
    {
        Vector3 myPosition = targetPosition;
        myPosition.y += 30;
        myPosition.z -= 1;
        this.transform.position = myPosition;

        // SFX
        SoundManager.Instance.PlaySoundAt("Audio/SFX/SFX_Meteor", targetPosition);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isArrived && this.transform.position.y <= targetPosition.y)
        {
            //Debug.Log ("arrive");

            foreach (CircleCollider2D comp in this.GetComponents<CircleCollider2D>())
            {
                comp.enabled = true;
            }
            this.GetComponentInChildren<EllipsoidParticleEmitter>().Emit();
            //Camera shaking
            iTween.ShakePosition(Camera.main.gameObject, new Vector3(0.2f, 0.2f, 0f), 0.8f);
            isArrived = true;


        }
        else if (!isArrived)
        {
            Vector3 myPosition = this.transform.position;
            myPosition.y -= 30 * Time.deltaTime;
            this.transform.position = myPosition;

            //Lancer la disparition du meteor
            StartCoroutine(DisappearMeteor(3f));
        }
    }


    IEnumerator DisappearMeteor(float duration)
    {
        yield return new WaitForSeconds(duration);

        this.gameObject.GetComponent<FadeOut>().fadeOut(2);
        meteorShadow.gameObject.GetComponent<FadeOut>().fadeOut(2);

        //Debug.Log ("stunEndTimestamp Stun");
        StartCoroutine(DisableMeteor(2f));
    }

    IEnumerator DisableMeteor(float duration)
    {
        yield return new WaitForSeconds(duration);

        this.gameObject.SetActive(false);

        this.gameObject.SetActive(false);
        meteorShadow.gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //If other = player => l'ecraser
        //A tester

        if (other.tag == "Player")
        {
            //Debug.Log ("Collider ecrase !");
            other.GetComponent<CharModel>().SetStuned(2f);
            //Perte d'une vie
            GameObject levelController = GameObject.Find("LevelController");
            levelController.GetComponent<LevelController>().Error("Meteorite Hit player : " + other.name);
        }
    }


}
