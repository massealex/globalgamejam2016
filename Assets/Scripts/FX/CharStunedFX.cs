﻿using UnityEngine;
using System.Collections;

public class CharStunedFX : MonoBehaviour {
	private bool isPlaying = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(isPlaying)
			this.transform.Rotate (0f, 0f, 50f * Time.deltaTime);
	}

	public void SetPlayingStuned(bool isPlaying) {
		this.isPlaying = isPlaying;
		foreach (ParticleSystem stunParticleSystem in this.GetComponentsInChildren<ParticleSystem>()) 
		{
			stunParticleSystem.enableEmission = isPlaying;
		}
	}

}
